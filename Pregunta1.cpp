#include <iostream>
#include<windows.h>

using namespace std;
struct nodo
{
    char nro;
    struct nodo *sgte;
};
struct cola
{
    nodo *delante;
    nodo *atras  ;
};
void encolar( struct cola &q, char valor )
{
     struct nodo *aux = new(struct nodo);

     aux->nro = valor;
     aux->sgte = NULL;

     if( q.delante == NULL)
         q.delante = aux;   
     else
         (q.atras)->sgte = aux;

     q.atras = aux;        

}

void muestraCola( struct cola q )
{
     struct nodo *aux;

     aux = q.delante;

     while( aux != NULL )
     {
            cout<<"   "<< aux->nro ;
            aux = aux->sgte;
     }
}

void menu()
{
    cout<<"\n\t IMPLEMENTACION DE COLAS EN C++\n\n";
    cout<<" 1. PUSH                               "<<endl;
    cout<<" 2. MOSTRAR COLA                          "<<endl;
    cout<<" 3. SALIR                                 "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

int main()
{
    struct cola q;

    q.delante = NULL;
    q.atras   = NULL;

    char dato;
    int op;    
    int x ;   

    do
    {
        menu();  cin>> op;

        switch(op)
        {
            case 1:

                 cout<< "\n LETRA A ENCOLAR: "; cin>> dato;
                 encolar( q, dato );
                 cout<<"\n\n\t\tLetra " << dato << " encolado...\n\n";
            break;


            case 2:

                 cout << "\n\n MOSTRANDO COLA\n\n";
                 if(q.delante!=NULL) muestraCola( q );
                 else   cout<<"\n\n\tCola vacia...!"<<endl;
            break;

         }

        cout<<endl<<endl;
        system("pause");
        system("cls");

    }while(op!=5);


    return 0;
}


